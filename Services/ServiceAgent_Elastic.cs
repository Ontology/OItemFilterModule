﻿using OItemFilterModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemFilterModule.Services
{
    public class ServiceAgent_Elastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private OntologyModDBConnector dbReaderOItems;

        public clsOntologyItem GetOItem(string id, string type)
        {
            return dbReaderOItems.GetOItem(id, type);
        }

        public List<clsOntologyItem> ClassListFiltered { get; private set; }

        private clsOntologyItem resultFilteredClassList;
        public clsOntologyItem ResultFilteredClassList
        {
            get { return resultFilteredClassList; }
            set
            {
                resultFilteredClassList = value;
                RaisePropertyChanged(nameof(ResultFilteredClassList));
            }
        }

        private clsOntologyItem resultAttributeTypeList;
        public clsOntologyItem ResultAttributeTypeList
        {
            get { return resultAttributeTypeList; }
            set
            {
                resultAttributeTypeList = value;
                RaisePropertyChanged(nameof(ResultAttributeTypeList));
            }
        }

        public List<clsObjectAtt> AttributeList { get; private set; }

        private clsOntologyItem resultAttributeList;
        public clsOntologyItem ResultAttributeList
        {
            get { return resultAttributeList; }
            set
            {
                resultAttributeList = value;
                RaisePropertyChanged(nameof(ResultAttributeList));
            }
        }

        public LookupSimpleItem ClassList { get; private set; }

        private clsOntologyItem resultClassList;
        public clsOntologyItem ResultClassList
        {
            get { return resultClassList; }
            set
            {
                resultClassList = value;
                RaisePropertyChanged(nameof(ResultClassList));
            }
        }

        public LookupSimpleItem AttributeTypeList { get; private set; }

        private clsOntologyItem resultRelationTypeList;
        public clsOntologyItem ResultRelationTypeList
        {
            get { return resultRelationTypeList; }
            set
            {
                resultRelationTypeList = value;
                RaisePropertyChanged(nameof(ResultRelationTypeList));
            }
        }

        public LookupSimpleItem RelationTypeList { get; private set; }

        public List<clsOntologyItem> ObjectList { get; private set; }

        private clsOntologyItem resultObjectList;
        public clsOntologyItem ResultObjectList
        {
            get { return resultObjectList; }
            set
            {
                resultObjectList = value;
                RaisePropertyChanged(nameof(ResultObjectList));
            }
        }

        public LookupRelItem ObjectRelList { get; private set; }

        private clsOntologyItem resultObjectRelList;
        public clsOntologyItem ResultObjectRelList
        {
            get { return resultObjectRelList; }
            set
            {
                resultObjectRelList = value;
                RaisePropertyChanged(nameof(ResultObjectRelList));
            }
        }


        public async Task<clsOntologyItem> GetClassList(List<clsOntologyItem> classSearch, bool triggerResult = true)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);
            var result = dbReader.GetDataClasses(classSearch);

            ClassListFiltered = dbReader.Classes1;
            if (triggerResult)
            {
                ResultFilteredClassList = result;
            }
            
            return result;
        }

        public async Task<LookupSimpleItem> GetClasses(SearchItem searchItem)
        {
            var dbReaderObjects = new OntologyModDBConnector(localConfig.Globals);
            var result = dbReaderObjects.GetDataClasses();

            LookupSimpleItem lookupItem = null;
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                lookupItem = new LookupSimpleItem
                {
                    LookupItems = dbReaderObjects.Classes1,
                    SearchItem = searchItem
                };


            }

            ClassList = lookupItem;
            ResultClassList = result;
            return lookupItem;
        }

        public async Task<clsOntologyItem> GetObjectList(List<clsOntologyItem> objectSearch)
        {
            var dbReaderObjects = new OntologyModDBConnector(localConfig.Globals);
            var result = dbReaderObjects.GetDataObjects(objectSearch);

            ObjectList = dbReaderObjects.Objects1;
            ResultObjectList = result;
            return result;
        }

        public async Task<clsOntologyItem> GetAttributeList(List<clsObjectAtt> attSearch)
        {
            var dbReaderAttributes = new OntologyModDBConnector(localConfig.Globals);
            var result = dbReaderAttributes.GetDataObjectAtt(attSearch);

            AttributeList = dbReaderAttributes.ObjAtts;
            ResultAttributeList = result;

            return result;

        }

        public async Task<LookupSimpleItem> GetAttributeTypes(SearchItem searchItem)
        {
            var dbReaderObjects = new OntologyModDBConnector(localConfig.Globals);
            var result = dbReaderObjects.GetDataAttributeType();

            LookupSimpleItem lookupItem = null;
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                lookupItem = new LookupSimpleItem
                {
                    LookupItems = dbReaderObjects.AttributeTypes,
                    SearchItem = searchItem
                };


            }

            AttributeTypeList = lookupItem;
            ResultAttributeTypeList = result;
            return lookupItem;
        }

        public async Task<LookupSimpleItem> GetRelationTypes(SearchItem searchItem)
        {
            var dbReaderObjects = new OntologyModDBConnector(localConfig.Globals);
            var result = dbReaderObjects.GetDataRelationTypes(null);

            LookupSimpleItem lookupItem = null;
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                lookupItem = new LookupSimpleItem
                {
                    LookupItems = dbReaderObjects.RelationTypes,
                    SearchItem = searchItem
                };


            }

            RelationTypeList = lookupItem;
            ResultRelationTypeList = result;
            return lookupItem;
        }

        public async Task<clsOntologyItem> GetObjectRelations(List<clsObjectRel> search, clsOntologyItem direction)
        {
            var dbReaderObjectRels = new OntologyModDBConnector(localConfig.Globals);
            var result = dbReaderObjectRels.GetDataObjectRel(search);

            ObjectRelList = new LookupRelItem
            {
                Direction = direction,
                LookupItems = dbReaderObjectRels.ObjectRels
            };

            ResultObjectRelList = result;
            return result;
        }

        public ServiceAgent_Elastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            dbReaderOItems = new OntologyModDBConnector(localConfig.Globals);

            ClassListFiltered = new List<clsOntologyItem>();
            ObjectList = new List<clsOntologyItem>();
        }

    }

    public class LookupSimpleItem
    {
        public List<clsOntologyItem> LookupItems { get; set; }
        public SearchItem SearchItem { get; set; }
    }

    public class LookupRelItem
    {
        public List<clsObjectRel> LookupItems { get; set; }
        public clsOntologyItem Direction { get; set; }
    }
}
