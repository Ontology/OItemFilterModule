﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OItemFilterModule.Notifications {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class NotifyChanges {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal NotifyChanges() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("OItemFilterModule.Notifications.NotifyChanges", typeof(NotifyChanges).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Text_View.
        /// </summary>
        internal static string View_Text_View {
            get {
                return ResourceManager.GetString("View_Text_View", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DataText_Class.
        /// </summary>
        internal static string ViewModel_DataText_Class {
            get {
                return ResourceManager.GetString("ViewModel_DataText_Class", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsEnabled_AddObject.
        /// </summary>
        internal static string ViewModel_IsEnabled_AddObject {
            get {
                return ResourceManager.GetString("ViewModel_IsEnabled_AddObject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsEnabled_Apply.
        /// </summary>
        internal static string ViewModel_IsEnabled_Apply {
            get {
                return ResourceManager.GetString("ViewModel_IsEnabled_Apply", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsEnabled_Apply.
        /// </summary>
        internal static string ViewModel_IsEnabled_Apply1 {
            get {
                return ResourceManager.GetString("ViewModel_IsEnabled_Apply1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsEnabled_NullObject.
        /// </summary>
        internal static string ViewModel_IsEnabled_NullObject {
            get {
                return ResourceManager.GetString("ViewModel_IsEnabled_NullObject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsEnabled_RemoveObject.
        /// </summary>
        internal static string ViewModel_IsEnabled_RemoveObject {
            get {
                return ResourceManager.GetString("ViewModel_IsEnabled_RemoveObject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsSuccessful_Login.
        /// </summary>
        internal static string ViewModel_IsSuccessful_Login {
            get {
                return ResourceManager.GetString("ViewModel_IsSuccessful_Login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsToggled_Listen.
        /// </summary>
        internal static string ViewModel_IsToggled_Listen {
            get {
                return ResourceManager.GetString("ViewModel_IsToggled_Listen", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsToggled_Listen.
        /// </summary>
        internal static string ViewModel_IsToggled_Listen2 {
            get {
                return ResourceManager.GetString("ViewModel_IsToggled_Listen2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to KendoDataSource_Grid.
        /// </summary>
        internal static string ViewModel_KendoDataSource_Grid {
            get {
                return ResourceManager.GetString("ViewModel_KendoDataSource_Grid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Label_Class.
        /// </summary>
        internal static string ViewModel_Label_Class {
            get {
                return ResourceManager.GetString("ViewModel_Label_Class", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Label_Direction.
        /// </summary>
        internal static string ViewModel_Label_Direction {
            get {
                return ResourceManager.GetString("ViewModel_Label_Direction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Label_Object.
        /// </summary>
        internal static string ViewModel_Label_Object {
            get {
                return ResourceManager.GetString("ViewModel_Label_Object", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Label_RelationType.
        /// </summary>
        internal static string ViewModel_Label_RelationType {
            get {
                return ResourceManager.GetString("ViewModel_Label_RelationType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OFilterItem_RelationFilterItem.
        /// </summary>
        internal static string ViewModel_OFilterItem_RelationFilterItem {
            get {
                return ResourceManager.GetString("ViewModel_OFilterItem_RelationFilterItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ResultItem_AppliedItem.
        /// </summary>
        internal static string ViewModel_ResultItem_AppliedItem {
            get {
                return ResourceManager.GetString("ViewModel_ResultItem_AppliedItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ResultItem_ResultItem.
        /// </summary>
        internal static string ViewModel_ResultItem_ResultItem {
            get {
                return ResourceManager.GetString("ViewModel_ResultItem_ResultItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SearchItem_SearchItem.
        /// </summary>
        internal static string ViewModel_SearchItem_SearchItem {
            get {
                return ResourceManager.GetString("ViewModel_SearchItem_SearchItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SearchItemList_SearchItems.
        /// </summary>
        internal static string ViewModel_SearchItemList_SearchItems {
            get {
                return ResourceManager.GetString("ViewModel_SearchItemList_SearchItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Url_FilterObject.
        /// </summary>
        internal static string ViewModel_Url_FilterObject {
            get {
                return ResourceManager.GetString("ViewModel_Url_FilterObject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Url_Relation.
        /// </summary>
        internal static string ViewModel_Url_Relation {
            get {
                return ResourceManager.GetString("ViewModel_Url_Relation", resourceCulture);
            }
        }
    }
}
