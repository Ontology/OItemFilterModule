﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemFilterModule.Models
{
    [KendoGridConfig(height = "500px", width = "100%", groupbable = true, sortable = true, autoBind = true, selectable = "True", rowTemplate = "rowTemplate")]
    [KendoPageable(refresh = true, pageSizes = new int[] {5, 10, 15, 20, 50, 100, 1000}, buttonCount = 5, pageSize = 20)]
    [KendoFilter(extra = true)]
    [KendoStringFilterable(contains = "Contains", startsWith = "Starts with", eq = "Is equal to", neq = "Is not equal to", isnotnull = "Not null", isnull = "Null", isempty = "Empty")]
    public class ResultItem
    {
        [KendoColumn(hidden = false, title = "Apply", Order = 0, filterable = true, width = "100px")]
        public bool ApplyItem { get; set; }

        [KendoColumn(hidden = true)]
        public string IdItem { get; set; }

        [KendoColumn(hidden = false, title = "Name", Order = 1, filterable = true)]
        public string NameItem { get; set; }

        [KendoColumn(hidden = true)]
        public string IdParent { get; set; }

        [KendoColumn(hidden = false, title = "Parent", Order = 2, filterable = true)]
        public string NameParent { get; set; }

        [KendoColumn(hidden = true)]
        public string IdAttributeType { get; set; }

        [KendoColumn(hidden = false, title = "AttributeType", Order = 3, filterable = true)]
        public string NameAttributeType { get; set; }

        [KendoColumn(hidden = true)]
        public string IdAttribute { get; set; }

        [KendoColumn(hidden = false, title = "Value", Order = 4, filterable = true)]
        public string ValueNamed { get; set; }

        [KendoColumn(hidden = false, title = "Type", Order = 5, filterable = true)]
        public string Type { get; set; }
    }
}
