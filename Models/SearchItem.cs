﻿using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace OItemFilterModule.Models
{
    public delegate void SearchItemTextChanged();
    public class SearchItem
    {
        private Timer textChangeTimer;

        public event SearchItemTextChanged searchItemTextChanged;

        public string id { get; set; }
        public string type { get; set; }
        private string _searchText;
        public string searchText
        {
            get { return _searchText; }
            set
            {
                _searchText = value;
                textChangeTimer.Stop();
                if (!string.IsNullOrEmpty(_searchText))
                {
                    textChangeTimer.Start();
                }
            }
        }

        public string inputType { get; set; }
        public string idAttributeType { get; set; }
        public string idClass { get; set; }
        public string idRelationType { get; set; }

        public bool? boolValue { get; set; }
        public DateTime? dateTimeValue { get; set; }
        public long? intValue { get; set; }
        public double? dblValue { get; set; }
        public string strValue { get; set; }

        public bool leftSide { get; set; }
        public bool remove { get; set; }
        public bool clear { get; set; }
        public bool add { get; set; }
        public bool addAtttypeInput { get; set; }

        public KendoDropDownConfig dropDownConfig { get; set; }

        
        public SearchItem()
        {
            textChangeTimer = new Timer();
            textChangeTimer.Interval = 500;
            textChangeTimer.Elapsed += TextChangeTimer_Elapsed;
        }

        private void TextChangeTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            textChangeTimer.Stop();

            searchItemTextChanged?.Invoke();
        }
    }
}
