﻿using OItemFilterModule.Notifications;
using OItemFilterModule.Services;
using OItemFilterModule.Translations;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemFilterModule.Controllers
{
    public class OItemFilterViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        
        private string url_Relation;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.IFrame, ViewItemId = "iframeRelation", ViewItemType = ViewItemType.Content)]
		public string Url_Relation
        {
            get { return url_Relation; }
            set
            {
                if (url_Relation == value) return;

                url_Relation = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_Relation);

            }
        }

        private string label_RelationType;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblRelationType", ViewItemType = ViewItemType.Content)]
		public string Label_RelationType
        {
            get { return label_RelationType; }
            set
            {
                if (label_RelationType == value) return;

                label_RelationType = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_RelationType);

            }
        }

        private string label_Class;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblClass", ViewItemType = ViewItemType.Content)]
		public string Label_Class
        {
            get { return label_Class; }
            set
            {
                if (label_Class == value) return;

                label_Class = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Class);

            }
        }

        private string label_Object;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblObject", ViewItemType = ViewItemType.Content)]
		public string Label_Object
        {
            get { return label_Object; }
            set
            {
                if (label_Object == value) return;

                label_Object = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Object);

            }
        }

        private OFilterItem ofilteritem_RelationFilterItem;
		public OFilterItem OFilterItem_RelationFilterItem
        {
            get { return ofilteritem_RelationFilterItem; }
            set
            {
                if (ofilteritem_RelationFilterItem == value) return;

                if (value == null && ofilteritem_RelationFilterItem != null)
                {
                    ofilteritem_RelationFilterItem.PropertyChanged -= Ofilteritem_RelationFilterItem_PropertyChanged;
                }
                
                ofilteritem_RelationFilterItem = value;

                if (ofilteritem_RelationFilterItem != null)
                {
                    ofilteritem_RelationFilterItem.PropertyChanged += Ofilteritem_RelationFilterItem_PropertyChanged;
                }

                RaisePropertyChanged(NotifyChanges.ViewModel_OFilterItem_RelationFilterItem);

            }
        }

        private bool isenabled_Apply;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "applyItem", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Apply
        {
            get { return isenabled_Apply; }
            set
            {
                if (isenabled_Apply == value) return;

                isenabled_Apply = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Apply);

            }
        }

        private string label_Direction;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblDirection", ViewItemType = ViewItemType.Content)]
		public string Label_Direction
        {
            get { return label_Direction; }
            set
            {
                if (label_Direction == value) return;

                label_Direction = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Direction);

            }
        }

        private string url_FilterObject;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.IFrame, ViewItemId = "ifrmFilterObject", ViewItemType = ViewItemType.Content)]
		public string Url_FilterObject
        {
            get { return url_FilterObject; }
            set
            {
                if (url_FilterObject == value) return;

                url_FilterObject = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_FilterObject);

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(NotifyChanges.View_Text_View);

            }
        }

        private void Ofilteritem_RelationFilterItem_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(e.PropertyName);
        }
    }
}
