﻿using OItemFilterModule.Models;
using OItemFilterModule.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OItemFilterModule.Controllers
{
    public class OItemSearchViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(NotifyChanges.View_Text_View);

            }
        }

        private List<SearchItem> searchitemlist_SearchItems;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "searchItem", ViewItemType = ViewItemType.Other)]
		public List<SearchItem> SearchItemList_SearchItems
        {
            get { return searchitemlist_SearchItems; }
            set
            {
                if (searchitemlist_SearchItems == value) return;

                searchitemlist_SearchItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_SearchItemList_SearchItems);

            }
        }

        private SearchItem searchitem_SearchItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "searchItemChange", ViewItemType = ViewItemType.Other)]
		public SearchItem SearchItem_SearchItem
        {
            get { return searchitem_SearchItem; }
            set
            {

                searchitem_SearchItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_SearchItem_SearchItem);

            }
        }

        private Dictionary<string, object> kendoDatasource_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.DataSource)]
        public Dictionary<string, object> KendoDataSource_Grid
        {
            get { return kendoDatasource_Grid; }
            set
            {
                if (kendoDatasource_Grid == value) return;

                kendoDatasource_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_KendoDataSource_Grid);

            }
        }

        private ResultItem resultitem_ResultItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "resultItem", ViewItemType = ViewItemType.Other)]
		public ResultItem ResultItem_ResultItem
        {
            get { return resultitem_ResultItem; }
            set
            {
                if (resultitem_ResultItem == value) return;

                resultitem_ResultItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ResultItem_ResultItem);

            }
        }

        private bool isenabled_Apply;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "applyItem", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Apply
        {
            get { return isenabled_Apply; }
            set
            {

                isenabled_Apply = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Apply);

            }
        }

        private ResultItem resultitem_AppliedItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.Content)]
		public ResultItem ResultItem_AppliedItem
        {
            get { return resultitem_AppliedItem; }
            set
            {
                if (resultitem_AppliedItem == value) return;

                resultitem_AppliedItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ResultItem_AppliedItem);

            }
        }



    }
}
