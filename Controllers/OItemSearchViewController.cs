﻿using OItemFilterModule.Models;
using OItemFilterModule.Services;
using OItemFilterModule.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace OItemFilterModule.Controllers
{
    public class OItemSearchViewController : OItemSearchViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ServiceAgent_Elastic serviceAgentElastic;

        private clsLocalConfig localConfig;

        private List<ResultItem> resultItems;

        private JsonFactory jsonFactory;

        List<clsOntologyItem> classSearch = new List<clsOntologyItem>();
        List<clsOntologyItem> objectSearch = new List<clsOntologyItem>();
        List<clsOntologyItem> relationTypeSearch = new List<clsOntologyItem>();
        List<clsOntologyItem> attributeTypeSearch = new List<clsOntologyItem>();
        List<clsObjectRel> objectRelSearch = new List<clsObjectRel>();
        List<clsObjectAtt> objectAttSearch = new List<clsObjectAtt>();

        List<string> namesList = new List<string>();

        private List<ResultItem> appliedResultItems = new List<ResultItem>();

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public OItemSearchViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += OItemSearchViewController_PropertyChanged;
        }

        private void OItemSearchViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (e.PropertyName == nameof(ResultItem_AppliedItem))
            {
                if (ResultItem_AppliedItem != null)
                {
                    var resultItem = resultItems.FirstOrDefault(resultItm => resultItm.IdItem == ResultItem_AppliedItem.IdItem);

                    if (resultItem == null)
                    {
                        ResultItem_AppliedItem.ApplyItem = false;
                    }
                    else
                    {
                        resultItem.ApplyItem = ResultItem_AppliedItem.ApplyItem;
                        if (resultItem.ApplyItem)
                        {
                            appliedResultItems.Add(new ResultItem
                            {
                                IdItem = ResultItem_AppliedItem.IdItem
                            });
                        }
                        else
                        {
                            appliedResultItems.RemoveAll(appliedItem => appliedItem.IdItem == ResultItem_AppliedItem.IdItem);

                        }

                        IsEnabled_Apply = appliedResultItems.Any();
                    }

                    
                }
            }

            if (e.PropertyName == nameof(KendoDataSource_Grid))
            {
                appliedResultItems.Clear();
            }


            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new ServiceAgent_Elastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

            jsonFactory = new JsonFactory();
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });


            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ServiceAgent_Elastic.ResultFilteredClassList))
            {
                if (serviceAgentElastic.ResultFilteredClassList.GUID == localConfig.Globals.LState_Success.GUID)
                {

                    var result1 = serviceAgentElastic.GetObjectList(objectSearch);
                    
                }
                
            }
            else if (e.PropertyName == nameof(ServiceAgent_Elastic.ResultClassList))
            {
                serviceAgentElastic.ClassList.SearchItem.dropDownConfig = new KendoDropDownConfig
                {
                    dataSource = serviceAgentElastic.ClassList.LookupItems.OrderBy(lookItm => lookItm.Name).Select(cls => new KendoDropDownItem
                    {
                        Value = cls.GUID,
                        Text = cls.Name
                    }).ToList(),
                    optionLabel = "Class"

                };

                SearchItem_SearchItem = serviceAgentElastic.ClassList.SearchItem;
            }
            else if (e.PropertyName == nameof(ServiceAgent_Elastic.ResultObjectList))
            {
                if (serviceAgentElastic.ResultObjectList.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    resultItems = new List<ResultItem>();

                    if (SearchItemList_SearchItems.Any(searchItm => searchItm.type == "classButton"))
                    {
                        resultItems = (from obj in serviceAgentElastic.ObjectList
                                       join cls in serviceAgentElastic.ClassList.LookupItems on obj.GUID_Parent equals cls.GUID
                                       select new ResultItem
                                       {
                                           IdItem = obj.GUID,
                                           NameItem = obj.Name,
                                           IdParent = obj.GUID_Parent,
                                           NameParent = cls.Name,
                                           Type = localConfig.Globals.Type_Object
                                       }).ToList();
                    }
                    else
                    {
                        var classSearch = serviceAgentElastic.ObjectList.GroupBy(obj => new { GUID_Parent = obj.GUID_Parent }).Select(objGrp => new clsOntologyItem
                        {
                            GUID = objGrp.Key.GUID_Parent
                        }).ToList();

                        if (classSearch.Any())
                        {
                            var resultTask1 = serviceAgentElastic.GetClassList(classSearch, false);
                            resultTask1.Wait();

                        }
                        resultItems = (from obj in serviceAgentElastic.ObjectList
                                       join cls in serviceAgentElastic.ClassListFiltered on obj.GUID_Parent equals cls.GUID
                                       select new ResultItem
                                       {
                                           IdItem = obj.GUID,
                                           NameItem = obj.Name,
                                           IdParent = obj.GUID_Parent,
                                           NameParent = cls.Name,
                                           Type = localConfig.Globals.Type_Object
                                       }).ToList();
                    }
                    
                    
                    var sessionFile = webSocketServiceAgent.RequestWriteStream(Guid.NewGuid().ToString() + ".json");

                    var resultTask = GridFactory.WriteKendoJson(typeof(ResultItem), resultItems.ToList<object>(), sessionFile);

                    resultTask.Wait();

                    if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        KendoDataSource_Grid = GridFactory.CreateKendoGridConfig(typeof(ResultItem), sessionFile);
                    }
                }
            }
            else if (e.PropertyName == nameof(ServiceAgent_Elastic.ResultAttributeTypeList))
            {
                if (serviceAgentElastic.ResultAttributeTypeList.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    serviceAgentElastic.AttributeTypeList.SearchItem.dropDownConfig = new KendoDropDownConfig
                    {
                        dataSource = serviceAgentElastic.AttributeTypeList.LookupItems.OrderBy(lookItm => lookItm.Name).Select(attType => new KendoDropDownItem
                        {
                            Value = attType.GUID,
                            Text = attType.Name
                        }).ToList(),
                        optionLabel = "Attribute-Type"

                    };

                    SearchItem_SearchItem = serviceAgentElastic.AttributeTypeList.SearchItem;
                }
            }
            else if (e.PropertyName == nameof(ServiceAgent_Elastic.ResultRelationTypeList))
            {
                if (serviceAgentElastic.ResultRelationTypeList.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    serviceAgentElastic.RelationTypeList.SearchItem.dropDownConfig = new KendoDropDownConfig
                    {
                        dataSource = serviceAgentElastic.RelationTypeList.LookupItems.OrderBy(lookItm => lookItm.Name).Select(attType => new KendoDropDownItem
                        {
                            Value = attType.GUID,
                            Text = attType.Name
                        }).ToList(),
                        optionLabel = "Relation-Type"

                    };

                    SearchItem_SearchItem = serviceAgentElastic.RelationTypeList.SearchItem;
                }
            }
            else if (e.PropertyName == nameof(ServiceAgent_Elastic.ResultAttributeList))
            {
                resultItems = (from obj in serviceAgentElastic.AttributeList
                               select new ResultItem
                               {
                                   IdItem = obj.ID_Object,
                                   NameItem = obj.Name_Object,
                                   IdParent = obj.ID_Class,
                                   NameParent = obj.Name_Class,
                                   IdAttribute = obj.ID_Attribute,
                                   IdAttributeType = obj.ID_AttributeType,
                                   NameAttributeType = obj.Name_AttributeType,
                                   ValueNamed = obj.Val_Named,
                                   Type = localConfig.Globals.Type_ObjectAtt
                               }).ToList();

                var sessionFile = webSocketServiceAgent.RequestWriteStream(Guid.NewGuid().ToString() + ".json");

                var resultTask = GridFactory.WriteKendoJson(typeof(ResultItem), resultItems.ToList<object>(), sessionFile);

                resultTask.Wait();

                if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    KendoDataSource_Grid = GridFactory.CreateKendoGridConfig(typeof(ResultItem), sessionFile);
                }
            }
            else if (e.PropertyName == nameof(ServiceAgent_Elastic.ResultObjectRelList))
            {
                if (serviceAgentElastic.ObjectRelList.Direction.GUID == localConfig.Globals.Direction_LeftRight.GUID)
                {
                    var objectRels = serviceAgentElastic.ObjectRelList.LookupItems;

                    if (namesList.Any())
                    {
                        objectRels = (from rel in objectRels
                                      from name in namesList
                                      where rel.Name_Other.ToLower().Contains(name)
                                      select rel).ToList();
                    }

                    resultItems = objectRels.Select(objRel => new ResultItem
                    {
                        IdItem = objRel.ID_Object,
                        NameItem = objRel.Name_Object,
                        IdParent = objRel.ID_Parent_Object,
                        NameParent = objRel.Name_Parent_Object,
                        Type = localConfig.Globals.Type_ObjectRel
                    }).ToList();
                }
                else
                {
                    var objectRels = serviceAgentElastic.ObjectRelList.LookupItems;

                    if (namesList.Any())
                    {
                        objectRels = (from rel in objectRels
                                      from name in namesList
                                      where rel.Name_Object.ToLower().Contains(name)
                                      select rel).ToList();
                    }

                    resultItems = objectRels.Select(objRel => new ResultItem
                    {
                        IdItem = objRel.ID_Other,
                        NameItem = objRel.Name_Other,
                        IdParent = objRel.ID_Parent_Other,
                        NameParent = objRel.Name_Parent_Other,
                        Type = localConfig.Globals.Type_ObjectRel
                    }).ToList();

                }

                var sessionFile = webSocketServiceAgent.RequestWriteStream(Guid.NewGuid().ToString() + ".json");

                var resultTask = GridFactory.WriteKendoJson(typeof(ResultItem), resultItems.ToList<object>(), sessionFile);

                resultTask.Wait();

                if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    KendoDataSource_Grid = GridFactory.CreateKendoGridConfig(typeof(ResultItem), sessionFile);
                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;


        }

       


        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
               if (webSocketServiceAgent.Command_RequestedCommand == "clicked.apply")
                {
                    var classes = appliedResultItems.Where(resultItm => resultItm.Type == localConfig.Globals.Type_Class).ToList();
                    var attributeTypes = appliedResultItems.Where(resultItm => resultItm.Type == localConfig.Globals.Type_AttributeType).ToList();
                    var relationTypes = appliedResultItems.Where(resultItm => resultItm.Type == localConfig.Globals.Type_RelationType).ToList();
                    var objects = appliedResultItems.Where(resultItm => resultItm.Type != localConfig.Globals.Type_Class &&
                            resultItm.Type != localConfig.Globals.Type_AttributeType &&
                            resultItm.Type != localConfig.Globals.Type_RelationType).ToList();

                    if (classes.Any())
                    {
                        var classItems = classes.Select(cls => serviceAgentElastic.GetOItem(cls.IdItem, localConfig.Globals.Type_Class)).ToList();
                        var message = new InterServiceMessage
                        {
                            ChannelId = Channels.AppliedClasses,
                            OItems = classItems
                        };

                        webSocketServiceAgent.SendInterModMessage(message);

                    }

                    if (attributeTypes.Any())
                    {
                        var attributeTypeItems = attributeTypes.Select(cls => serviceAgentElastic.GetOItem(cls.IdItem, localConfig.Globals.Type_AttributeType)).ToList();
                        var message = new InterServiceMessage
                        {
                            ChannelId = Channels.AppliedAttributeTypes,
                            OItems = attributeTypeItems
                        };

                        webSocketServiceAgent.SendInterModMessage(message);

                    }

                    if (relationTypes.Any())
                    {
                        var relationTypeItems = relationTypes.Select(cls => serviceAgentElastic.GetOItem(cls.IdItem, localConfig.Globals.Type_RelationType)).ToList();
                        var message = new InterServiceMessage
                        {
                            ChannelId = Channels.AppliedRelationTypes,
                            OItems = relationTypeItems
                        };

                        webSocketServiceAgent.SendInterModMessage(message);

                    }

                    if (objects.Any())
                    {
                        var objectItems = objects.Select(cls => serviceAgentElastic.GetOItem(cls.IdItem, localConfig.Globals.Type_Object)).ToList();
                        var message = new InterServiceMessage
                        {
                            ChannelId = Channels.AppliedObjects,
                            OItems = objectItems
                        };

                        webSocketServiceAgent.SendInterModMessage(message);

                    }
                }

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemId == "searchItem")
                    {
                        var searchItem = Newtonsoft.Json.JsonConvert.DeserializeObject<SearchItem>(changedItem.ViewItemValue.ToString());

                        var searchItemInList = SearchItemList_SearchItems.FirstOrDefault(searchItm => searchItm.id == searchItem.id);

                        if (searchItemInList != null)
                        {
                            if (searchItem.type == "attributeTypeButton")
                            {
                                var resultTask = serviceAgentElastic.GetAttributeTypes(searchItem);
                            }
                            else if (searchItem.type == "relationTypeButton")
                            {
                                var resultTask = serviceAgentElastic.GetRelationTypes(searchItem);
                            }
                            else if (searchItem.type == "classButton")
                            {
                                var resultTask = serviceAgentElastic.GetClasses(searchItem);
                            }
                            else
                            {
                                SearchItem_SearchItem = searchItem;
                            }
                            
                        }

                        searchItemInList.type = searchItem.type;
                        
                    }
                    else if (changedItem.ViewItemId == "searchItemToDel")
                    {
                        var searchItem = Newtonsoft.Json.JsonConvert.DeserializeObject<SearchItem>(changedItem.ViewItemValue.ToString());
                        

                        if (SearchItemList_SearchItems.Count > 1)
                        {
                            SearchItemList_SearchItems.RemoveAll(searchItm => searchItm.id == searchItem.id);
                            searchItem.remove = true;
                            
                        }
                        else
                        {
                            searchItem.clear = true;
                        }

                        SearchItem_SearchItem = searchItem;

                    }
                    else if (changedItem.ViewItemId == "searchItemToAdd")
                    {
                        var searchItem = Newtonsoft.Json.JsonConvert.DeserializeObject<SearchItem>(changedItem.ViewItemValue.ToString());

                        if (SearchItemList_SearchItems == null)
                        {
                            SearchItemList_SearchItems = new List<SearchItem>();
                        }

                        searchItem.add = true;
                        SearchItemList_SearchItems.Add(searchItem);

                        SearchItem_SearchItem = searchItem;

                    }
                    else if (changedItem.ViewItemId == "searchItemTextChanged")
                    {
                        var _searchItem = Newtonsoft.Json.JsonConvert.DeserializeObject<SearchItem>(changedItem.ViewItemValue.ToString());

                        if (_searchItem == null) return;

                        var searchItem = SearchItemList_SearchItems.FirstOrDefault(searchItm => searchItm.id == _searchItem.id);

                        if (searchItem == null) return;

                        searchItem.searchItemTextChanged -= SearchItem_searchItemTextChanged;
                        searchItem.searchItemTextChanged += SearchItem_searchItemTextChanged;
                        searchItem.searchText = _searchItem.searchText;
                    }
                    else if (changedItem.ViewItemId == "searchItemAttValue")
                    {
                        var _searchItem = Newtonsoft.Json.JsonConvert.DeserializeObject<SearchItem>(changedItem.ViewItemValue.ToString());

                        if (_searchItem == null ) return;

                        var searchItem = SearchItemList_SearchItems.FirstOrDefault(searchItm => searchItm.id == _searchItem.id);

                        if (searchItem == null) return;

                        searchItem.boolValue = _searchItem.boolValue;
                        searchItem.dateTimeValue = _searchItem.dateTimeValue;
                        searchItem.intValue = _searchItem.intValue;
                        searchItem.dblValue = _searchItem.dblValue;
                        searchItem.strValue = _searchItem.strValue;
                        SearchItem_searchItemTextChanged();

                    }
                    else if (changedItem.ViewItemId == "resultItemSelected")
                    {
                        ResultItem_ResultItem = Newtonsoft.Json.JsonConvert.DeserializeObject<ResultItem>(changedItem.ViewItemValue.ToString());

                        if (ResultItem_ResultItem.Type == localConfig.Globals.Type_Object || ResultItem_ResultItem.Type == localConfig.Globals.Type_ObjectAtt ||
                            ResultItem_ResultItem.Type == localConfig.Globals.Type_ObjectRel)
                        {
                            var oItemObject = serviceAgentElastic.GetOItem(ResultItem_ResultItem.IdItem, localConfig.Globals.Type_Object);

                            if (oItemObject != null)
                            {
                                var sendMessage = new InterServiceMessage
                                {
                                    ChannelId = Channels.ParameterList,
                                    OItems = new List<clsOntologyItem>
                                    {
                                        oItemObject
                                    }
                                };

                                webSocketServiceAgent.SendInterModMessage(sendMessage);
                            }
                        }
                    }
                    else if (changedItem.ViewItemClass == "KendoDropDownList" && changedItem.ViewItemType == "SelectedIndex")
                    {
                        if (string.IsNullOrEmpty(changedItem.ViewItemValue.ToString())) return;
                        var searchItem = SearchItemList_SearchItems.FirstOrDefault(searchItm => searchItm.id == changedItem.ViewItemId);
                        if (searchItem.type == "classButton")
                        {
                            searchItem.idClass = changedItem.ViewItemValue.ToString();
                            SearchItem_searchItemTextChanged();
                        }
                        if (searchItem.type == "attributeTypeButton")
                        {
                            var attributeType = serviceAgentElastic.GetOItem(changedItem.ViewItemValue.ToString(), localConfig.Globals.Type_AttributeType);
                            searchItem.idAttributeType = attributeType.GUID;
                            if (attributeType.GUID_Parent == localConfig.Globals.DType_Bool.GUID)
                            {
                                searchItem.inputType = localConfig.Globals.DType_Bool.Name;
                            }
                            else if (attributeType.GUID_Parent == localConfig.Globals.DType_DateTime.GUID)
                            {
                                searchItem.inputType = localConfig.Globals.DType_DateTime.Name;
                            }
                            else if (attributeType.GUID_Parent == localConfig.Globals.DType_Int.GUID)
                            {
                                searchItem.inputType = localConfig.Globals.DType_Int.Name;
                            }
                            else if (attributeType.GUID_Parent == localConfig.Globals.DType_Real.GUID)
                            {
                                searchItem.inputType = localConfig.Globals.DType_Real.Name;
                            }
                            else if (attributeType.GUID_Parent == localConfig.Globals.DType_String.GUID)
                            {
                                searchItem.inputType = localConfig.Globals.DType_String.Name;
                            }
                            searchItem.add = false;
                            searchItem.remove = false;
                            searchItem.clear = false;
                            searchItem.addAtttypeInput = true;
                            SearchItem_SearchItem = searchItem;
                        }
                        else if (searchItem.type == "relationTypeButton")
                        {
                            searchItem.idRelationType = changedItem.ViewItemValue.ToString();
                            SearchItem_searchItemTextChanged();
                        }
                    }
                    else if (changedItem.ViewItemId == "grid" && changedItem.ViewItemType == "Content")
                    {
                        var resultItem = Newtonsoft.Json.JsonConvert.DeserializeObject<ResultItem>(changedItem.ViewItemValue.ToString());



                        ResultItem_AppliedItem = resultItem;
                    }
                });

            }



        }

        
        private void SearchItem_searchItemTextChanged()
        {

            objectSearch.Clear();
            classSearch.Clear();
            attributeTypeSearch.Clear();
            relationTypeSearch.Clear();
            objectRelSearch.Clear();
            namesList.Clear();

            var direction = localConfig.Globals.Direction_LeftRight;

            if (SearchItemList_SearchItems.Any(searchItm => searchItm.type == "rightLeftButton"))
            {
                direction = localConfig.Globals.Direction_RightLeft;
            }

            SearchItemList_SearchItems.ForEach(searchItm =>
            {
                if (searchItm.type == "classButton")
                {
                    if (!string.IsNullOrEmpty(searchItm.idClass))
                    {
                        classSearch.Add(new clsOntologyItem
                        {
                            GUID_Parent = searchItm.idClass
                        });
                    }
                    
                }
                else if (searchItm.type == "objectButton")
                {
                    if (!string.IsNullOrEmpty(searchItm.searchText))
                    {
                        if (classSearch.Any())
                        {
                            objectSearch.AddRange(classSearch.Select(cls => new clsOntologyItem
                            {
                                GUID = localConfig.Globals.is_GUID(searchItm.searchText) ? searchItm.searchText : null,
                                Name = localConfig.Globals.is_GUID(searchItm.searchText) ? null : searchItm.searchText,
                                GUID_Parent = cls.GUID_Parent
                            }));
                        }
                        else
                        {
                            objectSearch.Add(new clsOntologyItem
                            {
                                GUID = localConfig.Globals.is_GUID(searchItm.searchText) ? searchItm.searchText : null,
                                Name = localConfig.Globals.is_GUID(searchItm.searchText) ? null : searchItm.searchText,
                            });
                        }
                    }
                }
                else if (searchItm.type == "relationTypeButton")
                {
                    relationTypeSearch.Add(new clsOntologyItem
                    {
                        GUID = searchItm.idRelationType
                    });
                }
                else if (searchItm.type == "attributeTypeButton")
                {
                    var attributeType = serviceAgentElastic.GetOItem(searchItm.idAttributeType, localConfig.Globals.Type_AttributeType);
                    if (attributeType.GUID_Parent == localConfig.Globals.DType_Bool.GUID)
                    {
                        attributeTypeSearch.Add(new clsOntologyItem
                        {
                            GUID = attributeType.GUID,
                            GUID_Parent = attributeType.GUID_Parent,
                            Val_Bool = searchItm.boolValue
                        });
                    }
                    else if (attributeType.GUID_Parent == localConfig.Globals.DType_DateTime.GUID)
                    {
                        attributeTypeSearch.Add(new clsOntologyItem
                        {
                            GUID = attributeType.GUID,
                            GUID_Parent = attributeType.GUID_Parent,
                            Val_Date = searchItm.dateTimeValue
                        });
                    }
                    else if (attributeType.GUID_Parent == localConfig.Globals.DType_Int.GUID)
                    {
                        attributeTypeSearch.Add(new clsOntologyItem
                        {
                            GUID = attributeType.GUID,
                            GUID_Parent = attributeType.GUID_Parent,
                            Val_Long = searchItm.intValue
                        });
                    }
                    else if (attributeType.GUID_Parent == localConfig.Globals.DType_Real.GUID)
                    {
                        attributeTypeSearch.Add(new clsOntologyItem
                        {
                            GUID = attributeType.GUID,
                            GUID_Parent = attributeType.GUID_Parent,
                            Val_Real = searchItm.dblValue
                        });
                    }
                    else if (attributeType.GUID_Parent == localConfig.Globals.DType_String.GUID)
                    {
                        attributeTypeSearch.Add(new clsOntologyItem
                        {
                            GUID = attributeType.GUID,
                            GUID_Parent = attributeType.GUID_Parent,
                            Val_String = searchItm.strValue
                        });
                    }
                }
                else if (searchItm.type == "leftRightButton")
                {
                    if (!string.IsNullOrEmpty(searchItm.searchText))
                    {
                        if (!localConfig.Globals.is_GUID(searchItm.searchText))
                        {
                            namesList.Add(searchItm.searchText.ToLower());
                        }
                        
                        if (classSearch.Any())
                        {
                            objectRelSearch.AddRange(classSearch.Select(cls => new clsObjectRel
                            {
                                ID_Other = localConfig.Globals.is_GUID(searchItm.searchText) ? searchItm.searchText : null,
                                Name_Other = localConfig.Globals.is_GUID(searchItm.searchText) ? null : searchItm.searchText,
                                ID_Parent_Other = cls.GUID_Parent
                            }));
                        }
                        else
                        {
                            objectRelSearch.Add(new clsObjectRel
                            {
                                ID_Other = localConfig.Globals.is_GUID(searchItm.searchText) ? searchItm.searchText : null,
                                Name_Other = localConfig.Globals.is_GUID(searchItm.searchText) ? null : searchItm.searchText,
                            });
                        }
                    }

                }
                else if (searchItm.type == "rightLeftButton")
                {
                    if (!string.IsNullOrEmpty(searchItm.searchText))
                    {
                        if (!localConfig.Globals.is_GUID(searchItm.searchText))
                        {
                            namesList.Add(searchItm.searchText.ToLower());
                        }

                        if (classSearch.Any())
                        {
                            objectRelSearch.AddRange(classSearch.Select(cls => new clsObjectRel
                            {
                                ID_Object = localConfig.Globals.is_GUID(searchItm.searchText) ? searchItm.searchText : null,
                                Name_Object = localConfig.Globals.is_GUID(searchItm.searchText) ? null : searchItm.searchText,
                                ID_Parent_Object = cls.GUID_Parent
                            }));
                        }
                        else
                        {
                            objectRelSearch.Add(new clsObjectRel
                            {
                                ID_Object = localConfig.Globals.is_GUID(searchItm.searchText) ? searchItm.searchText : null,
                                Name_Object = localConfig.Globals.is_GUID(searchItm.searchText) ? null : searchItm.searchText,
                            });
                        }
                    }
                }
            });

            if (attributeTypeSearch.Any())
            {
                List<clsObjectAtt> search = null;
                
                if (attributeTypeSearch.Any() && classSearch.Any() && objectSearch.Any())
                {
                    search = (from attTypeS in attributeTypeSearch
                              from objectS in objectSearch
                              from classS in classSearch
                              select new clsObjectAtt
                              {
                                  ID_AttributeType = attTypeS?.GUID,
                                  ID_Object = objectS?.GUID,
                                  ID_Class = classS?.GUID,
                                  Val_Named = attTypeS.GUID_Parent == localConfig.Globals.DType_Bool.GUID ? attTypeS.Val_Bool.ToString() : null,
                                  Val_Date = attTypeS.GUID_Parent == localConfig.Globals.DType_DateTime.GUID ? attTypeS.Val_Date :null,
                                  Val_Int  = attTypeS.GUID_Parent == localConfig.Globals.DType_Int.GUID ? attTypeS.Val_Long : null,
                                  Val_Real = attTypeS.GUID_Parent == localConfig.Globals.DType_Real.GUID ? attTypeS.Val_Real : null,
                                  Val_String = attTypeS.GUID_Parent == localConfig.Globals.DType_String.GUID ? attTypeS.Val_String : null
                              }).ToList();
                }
                else if (attributeTypeSearch.Any() && classSearch.Any())
                {
                    search = (from attTypeS in attributeTypeSearch
                              from classS in classSearch
                              select new clsObjectAtt
                              {
                                  ID_AttributeType = attTypeS?.GUID,
                                  ID_Class = classS?.GUID
                              }).ToList();
                }
                else if (attributeTypeSearch.Any() && objectSearch.Any())
                {
                    search = (from attTypeS in attributeTypeSearch
                              from objectS in objectSearch
                              select new clsObjectAtt
                              {
                                  ID_AttributeType = attTypeS?.GUID,
                                  ID_Object = objectS?.GUID,
                              }).ToList();
                }
                else if (attributeTypeSearch.Any())
                {
                    search = (from attTypeS in attributeTypeSearch
                              select new clsObjectAtt
                              {
                                  ID_AttributeType = attTypeS?.GUID,
                                  Val_Named = attTypeS.GUID_Parent == localConfig.Globals.DType_Bool.GUID ? attTypeS.Val_Bool.ToString() : null,
                                  Val_Date = attTypeS.GUID_Parent == localConfig.Globals.DType_DateTime.GUID ? attTypeS.Val_Date : null,
                                  Val_Int = attTypeS.GUID_Parent == localConfig.Globals.DType_Int.GUID ? attTypeS.Val_Long : null,
                                  Val_Real = attTypeS.GUID_Parent == localConfig.Globals.DType_Real.GUID ? attTypeS.Val_Real : null,
                                  Val_String = attTypeS.GUID_Parent == localConfig.Globals.DType_String.GUID ? attTypeS.Val_String : null
                              }).ToList();
                }
                else if (objectSearch.Any() && classSearch.Any())
                {
                    search = (from attTypeS in attributeTypeSearch
                              from classS in classSearch
                              select new clsObjectAtt
                              {
                                  ID_AttributeType = attTypeS?.GUID,
                                  ID_Class = classS?.GUID
                              }).ToList();
                }
                else if (objectSearch.Any())
                {
                    search = (from objectS in objectSearch
                              select new clsObjectAtt
                              { 
                                  ID_Object = objectS.GUID
                              }).ToList();
                }
                else if (classSearch.Any())
                {
                    search = (from classS in classSearch
                              select new clsObjectAtt
                              {
                                  ID_Class = classS?.GUID
                              }).ToList();
                }


                if (search.Any())
                {
                    var resultTask = serviceAgentElastic.GetAttributeList(search);
                }
                
            }
            else if (objectSearch.Any())
            {
                classSearch.Clear();
                var classesWithName = objectSearch.Where(obj => string.IsNullOrEmpty(obj.GUID_Parent) && !string.IsNullOrEmpty(obj.Name_Parent)).Select(obj => new clsOntologyItem
                {
                    Name = obj.Name_Parent
                }).ToList();

                if (classesWithName.Any())
                {
                    serviceAgentElastic.ClassListFiltered.Clear();
                    serviceAgentElastic.ObjectList.Clear();
                    var result = serviceAgentElastic.GetClassList(classesWithName);
                }
                else
                {
                    serviceAgentElastic.ClassListFiltered.Clear();
                    serviceAgentElastic.ObjectList.Clear();
                    var result = serviceAgentElastic.GetObjectList(objectSearch);
                }
                
                
            }
            else if (classSearch.Any())
            {
                var result = serviceAgentElastic.GetObjectList(classSearch);
            }
            else if (objectRelSearch.Any())
            {
                if (relationTypeSearch.Any())
                {
                    objectRelSearch = (from objRel in objectRelSearch
                                       from relType in relationTypeSearch
                                       select new clsObjectRel
                                       {
                                           ID_Object = objRel.ID_Object,
                                           Name_Object = objRel.Name_Object,
                                           ID_Other = objRel.ID_Other,
                                           Name_Other = objRel.Name_Other,
                                           ID_RelationType = relType.GUID
                                       }).ToList();
                }

                var result = serviceAgentElastic.GetObjectRelations(objectRelSearch, direction);
            }
            else if (relationTypeSearch.Any())
            {
                var search = relationTypeSearch.Select(relType => new clsObjectRel
                {
                    ID_RelationType = relType.GUID
                }).ToList();

                var result = serviceAgentElastic.GetObjectRelations(search, direction);
            }
        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {

            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

            
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
