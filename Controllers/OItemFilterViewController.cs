﻿using OItemFilterModule.Services;
using OItemFilterModule.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace OItemFilterModule.Controllers
{
    public class OItemFilterViewController : OItemFilterViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ServiceAgent_Elastic serviceAgent_Elastic;

        private clsLocalConfig localConfig;

        private List<string> subReceivers = new List<string>();

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public OItemFilterViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += OItemFilterViewController_PropertyChanged;
        }

        private void ValidateButtons()
        {
            

            
        }

        private bool CheckDataItem(string propertyName)
        {
            if (OFilterItem_RelationFilterItem != null && propertyName == OFilterItem_RelationFilterItem.NotifyIdClassItem)
            {
                var property = OFilterItem_RelationFilterItem.ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == propertyName);

                if (property == null) return false;

                property.ViewItem.AddValue(property.Property.GetValue(OFilterItem_RelationFilterItem));

                ValidateButtons();

                

                return true;
            }
            else if (OFilterItem_RelationFilterItem != null && propertyName == OFilterItem_RelationFilterItem.NotifyNameClassItem)
            {
                var property = OFilterItem_RelationFilterItem.ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == propertyName);

                if (property == null) return false;

                property.ViewItem.AddValue(property.Property.GetValue(OFilterItem_RelationFilterItem));

                webSocketServiceAgent.SendPropertyChange(propertyName, OFilterItem_RelationFilterItem);
                return true;
            }
            else if (OFilterItem_RelationFilterItem != null && propertyName == OFilterItem_RelationFilterItem.NotifyIdObjectItem)
            {
                var property = OFilterItem_RelationFilterItem.ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == propertyName);

                if (property == null) return false;

                property.ViewItem.AddValue(property.Property.GetValue(OFilterItem_RelationFilterItem));

                ValidateButtons();

                return true;
            }
            else if (OFilterItem_RelationFilterItem != null && propertyName == OFilterItem_RelationFilterItem.NotifyNameObjectItem)
            {
                var property = OFilterItem_RelationFilterItem.ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == propertyName);

                if (property == null) return false;

                property.ViewItem.AddValue(property.Property.GetValue(OFilterItem_RelationFilterItem));

                webSocketServiceAgent.SendPropertyChange(propertyName, OFilterItem_RelationFilterItem);
                return true;
            }
            else if (OFilterItem_RelationFilterItem != null && propertyName == OFilterItem_RelationFilterItem.NotifyIdRelationTypeItem)
            {
                var property = OFilterItem_RelationFilterItem.ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == propertyName);

                if (property == null) return false;

                property.ViewItem.AddValue(property.Property.GetValue(OFilterItem_RelationFilterItem));

                ValidateButtons();

                return true;
            }
            else if (OFilterItem_RelationFilterItem != null && propertyName == OFilterItem_RelationFilterItem.NotifyNameRelationTypeItem)
            {
                var property = OFilterItem_RelationFilterItem.ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == propertyName);

                if (property == null) return false;

                property.ViewItem.AddValue(property.Property.GetValue(OFilterItem_RelationFilterItem));

                webSocketServiceAgent.SendPropertyChange(propertyName, OFilterItem_RelationFilterItem);
                return true;
            }
            else if (OFilterItem_RelationFilterItem != null && propertyName == OFilterItem_RelationFilterItem.NotifyFilterNullItem)
            {
                var property = OFilterItem_RelationFilterItem.ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == propertyName);

                if (property == null) return false;

                property.ViewItem.AddValue(property.Property.GetValue(OFilterItem_RelationFilterItem));

                webSocketServiceAgent.SendPropertyChange(propertyName, OFilterItem_RelationFilterItem);
                return true;
            }
            else if (OFilterItem_RelationFilterItem != null && propertyName == OFilterItem_RelationFilterItem.NotifyIsDirectionLeftRight)
            {
                var property = OFilterItem_RelationFilterItem.ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == propertyName);

                if (property == null) return false;

                property.ViewItem.AddValue(property.Property.GetValue(OFilterItem_RelationFilterItem));

                webSocketServiceAgent.SendPropertyChange(propertyName, OFilterItem_RelationFilterItem);
                return true;
            }
            else if (OFilterItem_RelationFilterItem != null && (propertyName == OFilterItem_RelationFilterItem.NotifyIsEnabledAddObject ||
                propertyName == OFilterItem_RelationFilterItem.NotifyIsEnabledNullObject ||
                propertyName == OFilterItem_RelationFilterItem.NotifyIsEnabledRemoveObject))
            {
                var property = OFilterItem_RelationFilterItem.ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == propertyName);

                if (property == null) return false;

                property.ViewItem.AddValue(property.Property.GetValue(OFilterItem_RelationFilterItem));

                webSocketServiceAgent.SendPropertyChange(propertyName, OFilterItem_RelationFilterItem);

                IsEnabled_Apply = OFilterItem_RelationFilterItem.IsEnabledAddObject;
                return false;
            }
            





            return false;
        }

        private void OItemFilterViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (CheckDataItem(e.PropertyName)) return;

           

            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgent_Elastic = new ServiceAgent_Elastic(localConfig);
            serviceAgent_Elastic.PropertyChanged += ServiceAgent_Elastic_PropertyChanged;


        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.SelectedClassNode,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ReceiverOfDedicatedSender,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            Label_Object = translationController.Label_Object;
            Label_Class = translationController.Label_Class;
            Label_RelationType = translationController.Label_RelationType;
            Label_Direction = translationController.Label_Direction;

            IsEnabled_Apply = false;

            IsToggled_Listen = true;

            OFilterItem_RelationFilterItem = new OntoWebCore.Models.OFilterItem();
            OFilterItem_RelationFilterItem.IsEnabledAddObject = false;
            OFilterItem_RelationFilterItem.IsEnabledNullObject = false;
            OFilterItem_RelationFilterItem.IsEnabledRemoveObject = false;

            var relationTreeView = ModuleDataExchanger.GetViewById("1b980dcf4b504e43bf46b7916c889193");
            if (relationTreeView != null)
            {
                var url = webSocketServiceAgent.GetFileSystemObject().BaseUrl +
                    (webSocketServiceAgent.GetFileSystemObject().BaseUrl.EndsWith("/") || relationTreeView.NameCommandLineRun.StartsWith("/") ? "" : "/") +
                    relationTreeView.NameCommandLineRun + "?Sender=" + webSocketServiceAgent.EndpointId;

                Url_Relation = url;
            }
            
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void ServiceAgent_Elastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;


        }

        

        private void OpenObjectList()
        {
            if (OFilterItem_RelationFilterItem == null || string.IsNullOrEmpty(OFilterItem_RelationFilterItem.IdClassItem)) return;

            var objectListView = ModuleDataExchanger.GetViewById("f41336728e3a4c8382049d7b4edf7b80");
            if (objectListView != null)
            {
                var url = webSocketServiceAgent.GetFileSystemObject().BaseUrl +
                    (webSocketServiceAgent.GetFileSystemObject().BaseUrl.EndsWith("/") || objectListView.NameCommandLineRun.StartsWith("/") ? "" : "/") +
                    objectListView.NameCommandLineRun + "?Sender=" + webSocketServiceAgent.EndpointId +
                    "&Class=" + OFilterItem_RelationFilterItem.IdClassItem;

                Url_FilterObject = url;
            }
        }

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "ApplyFilter")
                {
                    ApplyFilter();
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "OpenObjectList")
                {
                    OpenObjectList();
                }


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    
                });
            }
          


        }

        private void ApplyFilter()
        {
            var interServiceMessage = new InterServiceMessage
            {
                ChannelId = Channels.ApplyFilter,
                GenericParameterItems = new List<object> { OFilterItem_RelationFilterItem }
            };

            webSocketServiceAgent.SendInterModMessage(interServiceMessage);
        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {

            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

            if (message.ChannelId == Channels.ReceiverOfDedicatedSender)
            {
                if (!subReceivers.Contains(message.SenderId))
                {
                    subReceivers.Add(message.SenderId);
                }
            }
            else if (message.ChannelId == Channels.AppliedObjects)
            {
                if (!subReceivers.Contains(message.SenderId)) return;

                if (message.OItems == null || message.OItems.Count != 1) return;

                var oItem = message.OItems.First();

                var objectItem = serviceAgent_Elastic.GetOItem(oItem.GUID, localConfig.Globals.Type_Object);

                if (objectItem == null) return;

                OFilterItem_RelationFilterItem.IdObjectItem = objectItem.GUID;
                OFilterItem_RelationFilterItem.NameObjectItem = objectItem.Name;

                webSocketServiceAgent.SendCommand("CloseFilterObjectWindow");
                Url_FilterObject = null;
            }
            else if (message.ChannelId == Channels.SelectedClassNode)
            {
                subReceivers.ForEach(rec =>
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = message.ChannelId,
                        ReceiverId = rec,
                        OItems = message.OItems.Select(oitm => oitm.Clone()).ToList()
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                });

                var oItemClass = message.OItems.First();
                if (string.IsNullOrEmpty(oItemClass.Name))
                {
                    oItemClass = serviceAgent_Elastic.GetOItem(oItemClass.GUID, localConfig.Globals.Type_Class);
                }
                if (oItemClass == null) return;

                Text_View = "(" + oItemClass.Name + ")";
            }
            else if (message.ChannelId == Channels.SelectedRelationNode)
            {
                var paramItem = message.GenericParameterItems.FirstOrDefault();

                if (paramItem == null) return;

                var relationNode = Newtonsoft.Json.JsonConvert.DeserializeObject<ObjectRelNode>(paramItem.ToString());

                if (relationNode == null) return;

                if (relationNode.NodeType == NodeType.NodeForwardFormal)
                {
                    var classId = relationNode.IdRight;
                    var relationTypeId = relationNode.IdRelationType;
                    var oItemClass = serviceAgent_Elastic.GetOItem(classId, localConfig.Globals.Type_Class);

                    if (oItemClass == null) return;

                    var oItemRelationType = serviceAgent_Elastic.GetOItem(relationTypeId, localConfig.Globals.Type_RelationType);

                    if (oItemRelationType == null) return;

                    OFilterItem_RelationFilterItem.IdClassItem = oItemClass.GUID;
                    OFilterItem_RelationFilterItem.NameClassItem = oItemClass.Name;

                    OFilterItem_RelationFilterItem.IdRelationTypeItem = oItemRelationType.GUID;
                    OFilterItem_RelationFilterItem.NameRelationTypeItem = oItemRelationType.Name;

                    OFilterItem_RelationFilterItem.IdDirectionItem = localConfig.Globals.Direction_LeftRight.GUID;
                    OFilterItem_RelationFilterItem.NameDirectionItem = localConfig.Globals.Direction_LeftRight.Name;

                   
                }
                else if (relationNode.NodeType == NodeType.NodeBackwardFormal)
                {
                    var classId = relationNode.IdLeft;
                    var relationTypeId = relationNode.IdRelationType;
                    var oItemClass = serviceAgent_Elastic.GetOItem(classId, localConfig.Globals.Type_Class);

                    if (oItemClass == null) return;

                    var oItemRelationType = serviceAgent_Elastic.GetOItem(relationTypeId, localConfig.Globals.Type_RelationType);

                    if (oItemRelationType == null) return;

                    OFilterItem_RelationFilterItem.IdClassItem = oItemClass.GUID;
                    OFilterItem_RelationFilterItem.NameClassItem = oItemClass.Name;

                    OFilterItem_RelationFilterItem.IdRelationTypeItem = oItemRelationType.GUID;
                    OFilterItem_RelationFilterItem.NameRelationTypeItem = oItemRelationType.Name;

                    OFilterItem_RelationFilterItem.IdDirectionItem = localConfig.Globals.Direction_RightLeft.GUID;
                    OFilterItem_RelationFilterItem.NameDirectionItem = localConfig.Globals.Direction_RightLeft.Name;

                    
                }
            }
        }


        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
